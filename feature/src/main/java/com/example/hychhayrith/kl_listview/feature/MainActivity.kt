package com.example.hychhayrith.kl_listview.feature

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var view = findViewById<ListView>(R.id.LV)
        // This is custom array list with arrayAdapter array
//        var studentArray = arrayListOf<String>("A", "B", "C")
//        var adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, studentArray)
//        view.adapter = adapter

        // Implement listView by using custom adapter
        var adapter = customAdapter(this)
        view.adapter = adapter

    }

    class customAdapter(context: Context): BaseAdapter(){
        var arrList = arrayListOf<String>(
                "Mr.A", "Mr.B", "Mr.C", "Mr.D"
        )
        private val myContext:Context
        init{
            myContext = context
        }
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            var inflaterX = LayoutInflater.from(myContext)
            var view = inflaterX.inflate(R.layout.custom_layout, parent, false)
            var name = view.findViewById<TextView>(R.id.name)
            name.text = studentContainer.studentArray.get(position).name
            var email = view.findViewById<TextView>(R.id.email)
            email.text = studentContainer.studentArray.get(position).email
            var phone = view.findViewById<TextView>(R.id.phone)
            phone.text = studentContainer.studentArray.get(position).mobile
            return view
        }

        override fun getItem(position: Int): Any {
            return ""
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return studentContainer.studentArray.count()
        }



    }
}
