package com.example.hychhayrith.kl_listview.feature

data class Student (var id: String, var name: String, var mobile: String, var email: String)

object studentContainer{
    var studentArray = listOf<Student>(
            Student("01", "Mr.A", "1289293", "A@gmail.com"),
            Student("02", "Mr.B", "999293", "B@gmail.com"),
            Student("03", "Mr.C", "121112", "C@gmail.com"),
            Student("04", "Mr.D", "128829", "D@gmail.com")
    )
}